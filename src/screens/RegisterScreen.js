import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Alert,
  ActivityIndicator,
} from "react-native";
import CButton from "../components/CButton";
import textStyles from "../theme/text";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import { colors } from "../theme/colors";
import { StringRegisterScreen } from "../constant/strings";
import CTextInput from "../components/CTextInput";
import BackButton from "../components/BackButton";
import AgreeNote from "../components/AgreeNote";
import statusBarStyle from "../theme/statusBar";
import axios from "axios";
import { API_URL_Register } from "../constant/api";

export default function RegisterScreen({ navigation }) {
  const [firstName, setFirstName] = useState("");
  const [lastNameName, setLastName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [password, setPassword] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const onPressRegisterButton = async () => {
    setIsLoading(true);
    console.log("Registering...");
    console.log("First Name: " + firstName);
    console.log("Last Name: " + lastNameName);
    console.log("Username: " + username);
    console.log("Email: " + email);
    console.log("Phone Number: " + phoneNumber);
    console.log("Address: " + address);
    console.log("Password: " + password);

    try {
      const userData = {
        firstName: firstName,
        lastName: lastNameName,
        username: username,
        email: email,
        phone: phoneNumber,
        password: password,
        role: "Deliverer",
        address: address,
        isActive: true,
      };

      const response = await axios.post(API_URL_Register, userData);
      console.log("STATUS: ", response.status);
      console.log("RESPONSE: ", response);
      console.log("----------------------------");

      if (response.status === 201) {
        // Registration successful
        console.log("Registration successful:", response.data);
        Alert.alert("Success!", "You have successfully registered!");
        navigation.replace("AppTabs");
      } else {
        // Registration failed
        console.error("Registration failed:", response.data);
        Alert.alert("Registration Failed", "Please try again later.");
      }
    } catch (error) {
      console.error("Error registering user:", error);
      Alert.alert("Error", "An error occurred while registering.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <SafeAreaView style={statusBarStyle.light}>
      <ScrollView bounces={true}>
        <View style={styles.container}>
          <View style={{ flexDirection: "column", alignItems: "flex-start" }}>
            <BackButton onPress={() => navigation.goBack()} />
            <View style={{ height: 40 }} />
            <Text style={textStyles.title}>{StringRegisterScreen.title}</Text>
            <Text style={textStyles.subtitle}>
              {StringRegisterScreen.subtitle}
            </Text>
            <CTextInput
              label="First Name"
              hint="Enter your first name"
              onChangeText={setFirstName}
            />
            <CTextInput
              label="Last Name"
              hint="Enter your last name"
              onChangeText={setLastName}
            />
            <CTextInput
              label="Username"
              hint="Enter your username"
              onChangeText={setUsername}
            />
            <CTextInput
              label="Email"
              hint="Enter your email"
              onChangeText={setEmail}
            />
            <CTextInput
              label="Phone Number"
              hint="Enter your phone number"
              onChangeText={setPhone}
            />
            <CTextInput
              label="Address"
              hint="Enter your address"
              onChangeText={setAddress}
            />
            <CTextInput
              label="Password"
              hint="6+ Characters, I Capital letter"
              onChangeText={setPassword}
            />
          </View>
          <View style={{ height: 20 }} />
          <CButton
            title="Register Now"
            onPress={onPressRegisterButton}
            disabled={isLoading}
          />
          {isLoading && (
            <View style={{ marginTop: 20 }}>
              <ActivityIndicator size="large" color={colors.primary} />
            </View>
          )}
          <View style={{ height: 20 }} />
          <AgreeNote />
          <View style={{ height: 20 }} />
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text style={textStyles.bodyText}> Already have an account?</Text>
            <View style={{ width: 10 }} />
            <CButton
              title="Login"
              isTextButton={true}
              onPress={() => navigation.replace("Login")}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
    paddingHorizontal: 20,
    marginTop: StatusBar.currentHeight || 0,
  },
});
