import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Alert, ActivityIndicator } from "react-native";
import CButton from "../components/CButton";
import textStyles from "../theme/text";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import { colors } from "../theme/colors";
import { StringLoginScreen } from "../constant/strings";
import CTextInput from "../components/CTextInput";
import BackButton from "../components/BackButton";
import statusBarStyle from "../theme/statusBar";
import axios from "axios";
import { API_URL_Login } from "../constant/api";
import useLoginStorage from "../hooks/useLoginStorage";

export default function LoginScreen({ navigation }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const { setLoginStatus } = useLoginStorage();

  const onPressLoginButton = async () => {
    setIsLoading(true);
    console.log("Login");
    console.log("Username: " + username);
    console.log("Password: " + password);

    try {
      const userData = {
        username: username,
        password: password,
      };

      const response = await axios.post(API_URL_Login, userData);

      if (response.status === 200) {
        // Login successful
        console.log("Login successful:", response.data);
        Alert.alert("Success!", "You have successfully logged in!");
        setLoginStatus(true);
        navigation.replace("AppTabs");
      } else {
        // Login failed
        console.error("Login failed:", response.data);
        Alert.alert("Login Failed", "Please check your credentials.");
      }
    } catch (error) {
      console.error("Error logging in:", error);
      Alert.alert("Error", "An error occurred while logging in.");
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <SafeAreaView style={statusBarStyle.light}>
      <View style={styles.container}>
        <View style={{ flexDirection: "column", alignItems: "flex-start" }}>
          <View style={{ height: 40 }} />
          <BackButton onPress={() => navigation.goBack()} />
          <Text style={textStyles.title}>{StringLoginScreen.title}</Text>
          <Text style={textStyles.subtitle}>{StringLoginScreen.subtitle}</Text>
          <CTextInput
            label="Username"
            hint="Enter your username"
            onChangeText={setUsername}
          />
          <CTextInput
            label="Password"
            hint="6+ Characters, I Capital letter"
            onChangeText={setPassword}
          />
          <CButton
            title="Forgot Password?"
            isTextButton={true}
            onPress={() => console.log("Forgot Password?")}
          />
        </View>
        <View style={{ height: 40 }} />
        <CButton
          title="Login"
          onPress={onPressLoginButton}
          disabled={isLoading}
        />
        {isLoading && (
          <View style={{ marginTop: 20 }}>
            <ActivityIndicator size="large" color={colors.primary} />
          </View>
        )}
        <View style={{ height: 20 }} />
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Text style={textStyles.bodyText}> Don`t have any account?</Text>
          <View style={{ width: 10 }} />
          <CButton
            title="Register"
            isTextButton={true}
            onPress={() => navigation.replace("Register")}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
    paddingHorizontal: 20,
    marginTop: StatusBar.currentHeight || 0,
  },
});
