import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import CButton from "../components/CButton";
import { colors } from "../theme/colors";
import { StringWelcomeScreen } from "../constant/strings";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import statusBarStyle from "../theme/statusBar";
import textStyles from "../theme/text";

export default function WelcomeScreen({ navigation }) {
  return (
    <SafeAreaView style={statusBarStyle.light}>
      <View style={styles.container}>
        <Image source={require("../../assets/logo.png")} style={styles.logo} />
        <Text style={textStyles.title}>Welcome</Text>
        <Text style={textStyles.bodyTextCenter}>
          {StringWelcomeScreen.description}
        </Text>
        <View style={{ height: 40 }} />
        <CButton title="Login" onPress={() => navigation.navigate("Login")} />
        <View style={{ height: 20 }} />
        <View style={{ flexDirection: "row" }}>
          <Text style={textStyles.bodyText}> Are you new here?</Text>
          <View style={{ width: 10 }} />
          <CButton
            title="Register"
            isTextButton={true}
            onPress={() => navigation.navigate("Register")}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    marginTop: StatusBar.currentHeight || 0,
  },
  logo: {
    width: 250,
    height: 80,
    resizeMode: "contain",
    marginBottom: 40,
  },
});
