import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { colors } from "../../theme/colors";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import statusBarStyle from "../../theme/statusBar";
import textStyles from "../../theme/text";
import CButton from "../../components/CButton";
import useLoginStorage from "../../hooks/useLoginStorage";

export default function AccountScreen({ navigation }) {
  const { setLoginStatus } = useLoginStorage();

  const logout = async () => {
    console.log("Logout");
    setLoginStatus(false);
    navigation.popToTop();
  };

  return (
    <SafeAreaView style={statusBarStyle.light}>
      <View style={styles.container}>
        <Text style={textStyles.title}>Account Screen</Text>
        <CButton title="Logout" onPress={logout} isTextButton={true} />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    marginTop: StatusBar.currentHeight || 0,
  },
  logo: {
    width: 250,
    height: 80,
    resizeMode: "contain",
    marginBottom: 40,
  },
});
