import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, FontAwesome, Entypo } from "@expo/vector-icons";
import HomeScreen from "./layout/HomeScreen";
import SettingsScreen from "./layout/SettingsScreen";
import AccountScreen from "./layout/AccountScreen";
import tabItemStyle from "../theme/tabItems";
import DeliveryScreen from "./layout/DeliveryScreen";

const Tab = createBottomTabNavigator();

export default function AppTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: tabItemStyle.focusColor,
        tabBarInactiveTintColor: tabItemStyle.defaultColor,
        tabBarActiveBackgroundColor: tabItemStyle.focusBackgroundColor,
        tabBarInactiveBackgroundColor: tabItemStyle.defaultBackgroundColor,
        tabBarStyle: {
          height: 60,
        },
        tabBarLabelStyle: {
          fontSize: tabItemStyle.labelSize,
          margin: tabItemStyle.labelMargin,
        },
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Entypo name="home" color={color} size={tabItemStyle.iconSize} />
          ),
        }}
      />
      <Tab.Screen
        name="Delivery"
        component={DeliveryScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <FontAwesome
              name="truck"
              color={color}
              size={tabItemStyle.iconSize}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Account"
        component={AccountScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons
              name="person"
              color={color}
              size={tabItemStyle.iconSize}
            />
          ),
        }}
      />
      {/* <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons
              name="settings"
              color={color}
              size={tabItemStyle.iconSize}
            />
          ),
        }}
      /> */}
    </Tab.Navigator>
  );
}
