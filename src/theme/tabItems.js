import { StyleSheet } from "react-native";
import { colors } from "./colors";

export default tabItemStyle = StyleSheet.create({
  focusColor: colors.primary,
  defaultColor: colors.tabDefault,
  iconSize: 24,
  labelSize: 12,
  labelMargin: 2,
});
