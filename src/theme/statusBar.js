import { StyleSheet } from "react-native";
import { colors } from "./colors";

export default statusBarStyle = StyleSheet.create({
  light: {
    backgroundColor: colors.background,
    flex: 1,
  },
});
