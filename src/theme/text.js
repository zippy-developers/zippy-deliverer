import { StyleSheet } from "react-native";
import { colors } from "./colors";

export default textStyles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "normal",
    marginBottom: 15,
    color: colors.textGray,
  },
  bodyText: {
    fontSize: 16,
    fontWeight: "normal",
    marginBottom: 10,
  },
  bodyTextCenter: {
    fontSize: 16,
    fontWeight: "normal",
    marginBottom: 10,
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.primary,
  },
  errorText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "red",
  },
  labelText: {
    fontSize: 14,
    fontWeight: "normal",
    marginBottom: 5,
  },
  placeholderText: {
    fontSize: 14,
    fontWeight: "normal",
    color: "gray",
  },
});
