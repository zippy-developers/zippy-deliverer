// add colors theme
export const colors = {
  primary: "#d84315",
  secondary: "#64748b",
  background: "#ffffff",
  // Text
  textBlack: "#1c2434",
  textGray: "#64748b",
  textWhite: "#ffffff",
  //Tab
  tabDefault: "#757575",
};
