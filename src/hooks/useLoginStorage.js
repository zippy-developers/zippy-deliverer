import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

const useLoginStorage = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    async function checkLoginStatus() {
      const loginStatus = await AsyncStorage.getItem("isLoggedIn");
      if (loginStatus === "true") {
        setIsLoggedIn(true);
      }
    }

    checkLoginStatus();
  }, []);

  const setLoginStatus = async (isLoggedIn) => {
    await AsyncStorage.setItem("isLoggedIn", isLoggedIn ? "true" : "false");
  };

  return { isLoggedIn, setLoginStatus };
};

export default useLoginStorage;
