import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function AgreeNote() {
  return (
    <View style={styles.container}>
      <Text>
        By joining, you agree to the Zippy Terms of Condition and to
        occasionally receive emails from us. Please read our Privacy Policy to
        learn how we use your personal data.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
});
