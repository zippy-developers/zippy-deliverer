import React from "react";

import { StyleSheet, Text, View } from "react-native";

export default function HelloWorld({ message }) {
  return (
    <View style={styles.container}>
      <Text>HelloWorld</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "yellow",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
