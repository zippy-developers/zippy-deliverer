import React from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import textStyles from "../theme/text";
import { colors } from "../theme/colors";

export default function BackButton({ onPress }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      {/* <Image
        style={styles.image}
        source={require("../../assets/icons/back.png")}
      /> */}
      <View style={{ flexDirection: "row", justifyContent: "center" }}>
        <Ionicons name="chevron-back" size={24} color={colors.primary} />
        <View style={{ width: 5 }} />
        <Text style={textStyles.buttonText}> Back</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
  },
  image: {
    width: 24,
    height: 24,
  },
});
