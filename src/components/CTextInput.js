import React from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { colors } from "../theme/colors";

export default function CTextInput({ label, hint, onChangeText }) {
  return (
    <View style={styles.container}>
      <Text style={styles.labelText}>{label}</Text>
      <TextInput
        style={styles.input}
        placeholder={hint}
        onChangeText={onChangeText}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  labelText: {
    fontSize: 16,
    fontWeight: "500",
    marginBottom: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: colors.textGray,
    borderRadius: 5,
    height: 50,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  placeholderText: {
    fontSize: 16,
    fontWeight: "normal",
    color: colors.textBlack,
  },
});
