import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { colors } from "../theme/colors";

export default function ButtonC({ title, onPress, isTextButton }) {
  return (
    <TouchableOpacity
      style={isTextButton ? styles.tButtonBG : styles.pButtonBG}
      onPress={onPress}
    >
      <Text style={isTextButton ? styles.tButtonText : styles.pButtonText}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  pButtonBG: {
    backgroundColor: colors.primary,
    padding: 10,
    borderRadius: 5,
    width: "100%",
  },
  tButtonBG: {},
  pButtonText: {
    color: colors.textWhite,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  tButtonText: {
    color: colors.primary,
    fontSize: 16,
    fontWeight: "500",
    textAlign: "center",
  },
});
