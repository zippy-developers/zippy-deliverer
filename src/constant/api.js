// https://api-v1-zippy.tharindu.me

export const API_URL = "https://api-v1-zippy.tharindu.me";
export const API_URL_AUTH = "https://api-v1-zippy.tharindu.me/api/auth";
export const API_URL_Register =
  "https://api-v1-zippy.tharindu.me/api/auth/register";
export const API_URL_Login = "https://api-v1-zippy.tharindu.me/api/auth/login";
