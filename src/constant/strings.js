export const StringWelcomeScreen = {
  title: "Welcome to the React Native",
  subtitle: "Start Screen",
  description:
    "Hello, welcome to the Zippy Delivery App. You can login or register to continue using our app.",
};

export const StringLoginScreen = {
  title: "Login",
  subtitle: "Ready to deliver at any time of the day or night.",
};

export const StringRegisterScreen = {
  title: "Register",
  subtitle:
    "With a growing population, there is a growing need for more delivery services.",
};
