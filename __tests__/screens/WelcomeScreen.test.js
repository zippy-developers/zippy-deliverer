import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import WelcomeScreen from "../../src/screens/WelcomeScreen";

describe("WelcomeScreen", () => {
  it("renders correctly and navigates to Login and Register screens", () => {
    const navigation = {
      navigate: jest.fn(),
    };

    const { getByText, getByTestId } = render(
      <WelcomeScreen navigation={navigation} />
    );

    const welcomeText = getByText("Welcome");
    const loginButton = getByText("Login");
    const registerButton = getByText("Register");

    expect(welcomeText).toBeTruthy();
    expect(loginButton).toBeTruthy();
    expect(registerButton).toBeTruthy();

    fireEvent.press(loginButton);
    expect(navigation.navigate).toHaveBeenCalledWith("Login");

    fireEvent.press(registerButton);
    expect(navigation.navigate).toHaveBeenCalledWith("Register");
  });
});
