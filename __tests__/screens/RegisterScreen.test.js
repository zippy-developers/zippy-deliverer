import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react-native";
import axios from "axios";
import RegisterScreen from "../../src/screens/RegisterScreen";
import CButton from "../../src/components/CButton";

jest.mock("axios");

describe("RegisterScreen", () => {
  it("renders correctly and handles registration", async () => {
    const navigation = { replace: jest.fn(), goBack: jest.fn() };
    const axiosPost = axios.post;

    axiosPost.mockResolvedValue({ status: 201, data: {} });

    const { getByText, getByPlaceholderText } = render(
      <RegisterScreen navigation={navigation} />
    );

    // Fill in the registration form
    const firstNameInput = getByPlaceholderText("Enter your first name");
    const lastNameInput = getByPlaceholderText("Enter your last name");
    const usernameInput = getByPlaceholderText("Enter your username");
    const emailInput = getByPlaceholderText("Enter your email");
    const phoneInput = getByPlaceholderText("Enter your phone number");
    const addressInput = getByPlaceholderText("Enter your address");
    const passwordInput = getByPlaceholderText(
      "6+ Characters, I Capital letter"
    );

    fireEvent.changeText(firstNameInput, "John");
    fireEvent.changeText(lastNameInput, "Doe");
    fireEvent.changeText(usernameInput, "johndoe");
    fireEvent.changeText(emailInput, "johndoe@example.com");
    fireEvent.changeText(phoneInput, "1234567890");
    fireEvent.changeText(addressInput, "123 Main St");
    fireEvent.changeText(passwordInput, "Password123");

    // Press the register button
    const registerButton = getByText("Register Now");
    fireEvent.press(registerButton);

    // const mockOnPress = jest.fn();
    // const isLoading = false;
    // const { getByTestId } = render(
    //   <CButton
    //     testID="registerButton"
    //     title="Register"
    //     onPress={mockOnPress}
    //     disabled={isLoading}
    //   />
    // );
    // const registerButton = getByTestId("registerButton");
    // fireEvent.press(registerButton);

    await waitFor(() => {
      //   expect(mockOnPress).toHaveBeenCalledTimes(1);
      expect(axiosPost).toHaveBeenCalledWith(
        "https://api-v1-zippy.tharindu.me/api/auth/register",
        {
          firstName: "John",
          lastName: "Doe",
          username: "johndoe",
          email: "johndoe@example.com",
          phone: "1234567890",
          password: "Password123",
          role: "Deliverer",
          address: "123 Main St",
          isActive: true,
        }
      );
    });

    // Expect navigation to have been called
    expect(navigation.replace).toHaveBeenCalledWith("AppTabs");
  });
});
