import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import CTextInput from "../../src/components/CTextInput";

describe("CTextInput", () => {
  it("renders correctly", () => {
    const { getByText, getByPlaceholderText } = render(
      <CTextInput label="Name" hint="Enter your name" onChangeText={() => {}} />
    );

    const label = getByText("Name");
    expect(label).toBeTruthy();

    const input = getByPlaceholderText("Enter your name");
    expect(input).toBeTruthy();
  });

  it("handles text input changes", () => {
    const onChangeTextMock = jest.fn();
    const { getByPlaceholderText } = render(
      <CTextInput
        label="Email"
        hint="Enter your email"
        onChangeText={onChangeTextMock}
      />
    );

    const input = getByPlaceholderText("Enter your email");
    fireEvent.changeText(input, "test@example.com");

    expect(onChangeTextMock).toHaveBeenCalledWith("test@example.com");
  });
});
