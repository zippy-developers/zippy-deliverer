import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import CButton from "../../src/components/CButton";

describe("CButton", () => {
  const onPressMock = jest.fn();
  const textButtonProps = {
    title: "Text Button",
    onPress: onPressMock,
    isTextButton: true,
  };

  const primaryButtonProps = {
    title: "Primary Button",
    onPress: onPressMock,
    isTextButton: false,
  };

  it("renders text button correctly", () => {
    const { getByText } = render(<CButton {...textButtonProps} />);

    const textButton = getByText("Text Button");
    expect(textButton).toBeTruthy();

    fireEvent.press(textButton);
    expect(onPressMock).toHaveBeenCalled();
  });

  it("renders primary button correctly", () => {
    const { getByText } = render(
      <CButton {...primaryButtonProps} />
    );

    const primaryButton = getByText("Primary Button");
    expect(primaryButton).toBeTruthy();

    fireEvent.press(primaryButton);
    expect(onPressMock).toHaveBeenCalled();
  });
});
