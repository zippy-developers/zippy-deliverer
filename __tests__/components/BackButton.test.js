import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import BackButton from "../../src/components/BackButton";

describe("BackButton", () => {
  it("renders correctly and calls onPress", () => {
    const onPressMock = jest.fn();
    const { getByText } = render(<BackButton onPress={onPressMock} />);

    const backButtonText = getByText("Back");
    expect(backButtonText).toBeTruthy();

    fireEvent.press(backButtonText);
    expect(onPressMock).toHaveBeenCalled();
  });
});
