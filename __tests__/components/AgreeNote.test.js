import React from "react";
import { render } from "@testing-library/react-native";
import AgreeNote from "../../src/components/AgreeNote";

describe("AgreeNote", () => {
  it("renders correctly", () => {
    const { getByText } = render(<AgreeNote />);

    const agreementText = getByText(
      "By joining, you agree to the Zippy Terms of Condition and to occasionally receive emails from us. Please read our Privacy Policy to learn how we use your personal data."
    );

    expect(agreementText).toBeTruthy();
  });
});
